---
title: "Getting started with Google Cloud Platform"
author: | 
        | Mark Yacoub
        | Duke Population Research Institute
output: html_document
---
  
```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE)
```

## Setup

This script installs the googleComputeEngineR package, sets up the environment, authenticates with the GCE API, and creates an RStduio VM instance.

Package installation needs to only be done once

```{r, eval = FALSE}
install.packages("googleComputeEngineR")
```

This sets up some R environment variables that googleComputeEngineR looks for to authenticate to your account when the package is loaded. R must restarted before the changes take effect.

```{r, eval = FALSE}
library(usethis)
edit_r_environ()
```

After the .Renviron file has opened, add the following lines

```{r, eval = FALSE}
GCE_AUTH_FILE = "file/path/to/account_key.json"
GCE_DEFAULT_PROJECT_ID = "project-id"
GCE_DEFAULT_ZONE = "us-east1-b"
```

Now we load the package and set the default project. This may be redundant since we just did it above. The final line prints the default project to make sure we did everything correctly.

```{r}
library(magrittr)
library(googleComputeEngineR)
default_project <- gce_get_project()
default_project$name
```

Let's check the project name and zone one last time.

```{r}
gce_get_global_project() # Again, checking the project name
gce_global_zone("us-east1-b") # Setting the default zone
```

This lists the GCE machine types you can select from.

```{r}
machines <- gce_list_machinetype() %$% items
head(machines)
```

## Deploying an instance

The name of the instance can be anything you choose. The template here is RStudio, but the package includes other templates as well. Set your own username and password so that you can log into the RStudio instance. The predefined_type argument allows you to select a machine with predefined CPUs and RAM. e2-micro is included in the free tier of GCP.

The VM will take a bit of time to run, and you will see the progress in the console. To access your VM, simply copy and paste the IP address the console spits out into your web browser. You may have to wait 10-15 minutes for the docker container to install. Use the username and password you provided to log in.

```{r}
vm <- gce_vm(name = "rstudio-test",
             template = "rstudio",
             username = "mark",
             password = "mark12345",
             predefined_type = "f1-micro")

vm
vm$status
```

You can list all your current instances.

```{r}
gce_list_instances()
```

Stop, start, and delete your instance. Remember to stop your instance when you are done with analysis, especially if you are using a paid tier.

```{r}
job <- gce_vm_stop("rstudio-test")
job <- gce_vm_start("rstudio-test")
gce_vm_delete("rstudio-test")
```
