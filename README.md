# Working with Big Data, SQL, and Cloud Computing using R

These materials accompany the DUPRI "Working with Big Data, SQL, and Cloud Computing using R" training. If you have any questions, contact Mark Yacoub at <mark.yacoub@duke.edu>.

* big_data_sql.Rmd - Rmarkdown HTML version of training code
* big_data_sql.html - HTML report of training for reference
* gcp_demo.Rmd - Rmarkdown HTML version of GCP demo
* gcp_demo.html - HTML report of GCP demo
* AWS_instructions.pdf - PDF instructions for getting started with AWS and RStudio
* GCP_instructions.pdf - PDF instructions for getting started with Google Cloud Platform and RStudio
* Durham_County_data_connecting.docx - Word document that explains how to connect to the Durham County data
* Big Data SQL.Rproj - RStudio project file
